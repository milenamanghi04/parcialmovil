using UnityEngine;

public class MoveCube : MonoBehaviour
{
    public float speed = 5f; // Velocidad del movimiento
    public float distance = 3f; // Distancia m�xima de movimiento desde el punto de inicio

    private Vector3 startPosition;
    private bool movingRight = true;

    void Start()
    {
        // Guardar la posici�n inicial del cubo
        startPosition = transform.position;
    }

    void Update()
    {
        // Calcular el nuevo desplazamiento basado en el tiempo
        float move = speed * Time.deltaTime;

        // Determinar la nueva posici�n del cubo
        if (movingRight)
        {
            transform.Translate(move, 0, 0);

            // Verificar si se ha alcanzado la distancia m�xima
            if (transform.position.x >= startPosition.x + distance)
            {
                movingRight = false;
            }
        }
        else
        {
            transform.Translate(-move, 0, 0);

            // Verificar si se ha alcanzado la distancia m�xima en la otra direcci�n
            if (transform.position.x <= startPosition.x - distance)
            {
                movingRight = true;
            }
        }
    }
}
