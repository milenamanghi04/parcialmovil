using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlEsferas : MonoBehaviour
{
    public GameObject[] prefabs; // Lista de prefabs que pueden aparecer
    public float tiempoMinimoEntreApariciones = 2f; // Tiempo m�nimo entre apariciones en segundos reales
    public float tiempoMaximoEntreApariciones = 5f; // Tiempo m�ximo entre apariciones en segundos reales
    public Vector3 posicion;
    public Camera camara;

    private GameObject objetoActivo; // Objeto actualmente activo

    void Start()
    {
        // Iniciar la aparici�n repetida de objetos
        InvokeRepeating("AparecerObjetoAleatorio", 0f, Random.Range(tiempoMinimoEntreApariciones, tiempoMaximoEntreApariciones));
    }
    void Update()
    {
        // Verificar si hay un objeto activo
        if (objetoActivo != null && camara != null)
        {
            // Obtener la posici�n del objeto activo
            Vector3 posicionObjeto = objetoActivo.transform.position;

            // Actualizar la posici�n de la c�mara para seguir al objeto activo
            camara.transform.position = new Vector3(posicionObjeto.x, camara.transform.position.y, camara.transform.position.z);
        }
    }

    public void AparecerObjetoAleatorio()
    {
        // Si ya hay un objeto activo, destruirlo
        if (objetoActivo != null)
        {
            Destroy(objetoActivo);
        }

        // Instanciar un prefab aleatorio de la lista
        int indicePrefab = Random.Range(0, prefabs.Length);
        objetoActivo = Instantiate(prefabs[indicePrefab], posicion, Quaternion.identity);
    }

    void OnCollisionEnter(Collision collision)
    {
        // Verificar si el objeto colisionado es el piso
        if (collision.gameObject.CompareTag("piso"))
        {
            AparecerObjetoAleatorio();
        }
    }
} 

