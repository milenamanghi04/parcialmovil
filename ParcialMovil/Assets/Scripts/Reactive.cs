using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reactive : MonoBehaviour
{
    void Start()
    {
        // Llamar a la funci�n para reactivar todos los objetos al inicio
        ReactivateAllObjects();
    }

    public void ReactivateAllObjects()
    {
        // Obtener la escena activa
        Scene currentScene = SceneManager.GetActiveScene();

        // Obtener todos los root game objects en la escena activa
        GameObject[] rootObjects = currentScene.GetRootGameObjects();

        foreach (GameObject obj in rootObjects)
        {
            // Reactivar el objeto y todos sus hijos
            SetActiveRecursively(obj, true);
        }
    }

    private void SetActiveRecursively(GameObject obj, bool state)
    {
        obj.SetActive(state);

        // Recorrer todos los hijos y activarlos tambi�n
        foreach (Transform child in obj.transform)
        {
            SetActiveRecursively(child.gameObject, state);
        }
    }
}
