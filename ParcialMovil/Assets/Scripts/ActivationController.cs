using UnityEngine;

public class ActivationController : MonoBehaviour
{
    public Camera mainCamera;
    public LayerMask layerMask;

    void Update()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(mainCamera);
        Collider[] colliders = Physics.OverlapSphere(mainCamera.transform.position, 100f, layerMask);

        foreach (Collider col in colliders)
        {
            if (GeometryUtility.TestPlanesAABB(planes, col.bounds))
            {
                if (!col.gameObject.activeInHierarchy)
                {
                    col.gameObject.SetActive(true);
                }
            }
        }
    }
}