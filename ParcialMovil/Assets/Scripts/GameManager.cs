using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int Puntos=0;
    public TextMeshProUGUI PuntajeText;
    public ControlVida controlvida;
    public GameTimer gameTimer;
    private int vidas = 3;

    public GameObject FinPartidaPanel;
    
    public TextMeshProUGUI PuntajeFinal;
    public TextMeshProUGUI TiempoFinal;


    void Start()
    {
        FinPartidaPanel.SetActive(false);
    }

  
    void Update()
    {
        PuntajeText.text = Puntos.ToString();
        
    }
    public void SumarPuntos()
    {
        Puntos++;

    }

    public void PerderVida()
    {
        vidas -= 1;
        controlvida.DesactivarVida(vidas);
        if(vidas == 0)
        {
            Reiniciar();
        }
    }
    public bool RecuperarVida()
    {
        if(vidas == 3)
        {
            return false;
        }
        controlvida.ActivarVida(vidas);
        vidas += 1;
        return true;
    }

    public void Reiniciar()
    {
        SceneManager.LoadScene(0);
    }

    public void FinPartida()
    {
        FinPartidaPanel.SetActive(true);
        PuntajeFinal.text = PuntajeText.text;
        TiempoFinal.text = gameTimer.TimeText.text + "s";
    }

  
}
