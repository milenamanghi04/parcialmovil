using UnityEngine;

public class ActivationZone : MonoBehaviour
{
    public float activationRadius = 50f;
    public Transform player;

    void start()
    {

        Collider[] objectsInRange = Physics.OverlapSphere(player.position, activationRadius);
        foreach (Collider col in objectsInRange)
        {
            GameObject obj = col.gameObject;
            if (!obj.activeInHierarchy)
            {
                obj.SetActive(false);
            }
        }
    }

    void Update()
    {
        Collider[] objectsInRange = Physics.OverlapSphere(player.position, activationRadius);
        foreach (Collider col in objectsInRange)
        {
            GameObject obj = col.gameObject;
            if (!obj.activeInHierarchy)
            {
                obj.SetActive(true);
            }
        }
    }
}