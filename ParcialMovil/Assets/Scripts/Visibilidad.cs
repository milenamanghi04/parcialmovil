using UnityEngine;

public class ActivateOnVisibility : MonoBehaviour
{
    private Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
        gameObject.SetActive(false);  // Desactiva el objeto al iniciar el juego
    }

    void OnBecameVisible()
    {
        gameObject.SetActive(true);  // Activa el objeto cuando se vuelve visible
    }

    void OnBecameInvisible()
    {
        if (gameObject.activeInHierarchy)
        {
            gameObject.SetActive(false);  // Desactiva el objeto cuando deja de ser visible
        }
    }
}