using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlayer : MonoBehaviour
{
    public GameManager gameManager;
    public Reactive reactive;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
    }
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag == "Obstaculo")
        {
            Handheld.Vibrate();
            gameManager.PerderVida();
            col.gameObject.SetActive(false);
        }
        if(col.gameObject.tag == "Punto")
        {
            gameManager.SumarPuntos();
            col.gameObject.SetActive(false);
        }
        if(col.gameObject.tag== "Meta")
        {
            gameManager.FinPartida();
        }
        if (col.gameObject.tag == "Vida")
        {
            bool vidaRecuperada = gameManager.RecuperarVida();
            if (vidaRecuperada)
            {
                col.gameObject.SetActive(false); 
            }
           

        }
    }



 

   
}
