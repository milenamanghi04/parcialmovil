using UnityEngine;

public class MecanicaCadena : MonoBehaviour
{
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Verifica si hay al menos un toque en la pantalla
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
            {
                StopVerticalMovement();
            }
            else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
            {
               
            }
        }
        else
        {
        
        }
    }

    void StopVerticalMovement()
    {
        // Establecer la velocidad en el eje Y a cero
        rb.velocity = new Vector3(rb.velocity.x, 0, rb.velocity.z);
    }

  
}