using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverConAcelerometro : MonoBehaviour
{
    // Velocidad de movimiento del objeto
    public float velocidad = 5.0f;

    void Update()
    {
        // Obtener la entrada del acelerómetro
        float movimientoHorizontal = Input.acceleration.x;

        // Calcular la velocidad de movimiento
        Vector3 movimiento = new Vector3(movimientoHorizontal, 0.0f, 0.0f);

        // Aplicar el movimiento al objeto
        transform.Translate(movimiento * velocidad * Time.deltaTime);
    }
}
