using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameTimer : MonoBehaviour
{
    private float elapsedTime = 0f;

    // Variable para mostrar el tiempo en la UI
    public TextMeshProUGUI TimeText;

    void Update()
    {
        // Incrementar el tiempo transcurrido
        elapsedTime += Time.deltaTime;

        // Actualizar el texto del temporizador en segundos


        int secondsElapsed = Mathf.FloorToInt(elapsedTime);
        // Actualizar el texto del UI con el tiempo en segundos sin comas
        TimeText.text =  secondsElapsed.ToString();

    }
}